 var ajaxmoudle = (function(){
	/**
	 * 封装ajax请求
	 * @param method 请求方法  必填  POST or GET
	 * @param url    请求url  必填  
	 * @param param  请求参数  必填  Object类型  或为  null
	 * @param callback 回调函数
	 * @param issync 是否异步  非必填  默认为true;
	 */
	function ajaxRequest(method,url,param,callback,issync){
		var sync = arguments.length > 4 ? issync : true;//如果参数个数大于4 则可以选择同步或者异步的请求方式
		param = param == null?{}:param;
		var request = getRequest();
		request.onreadystatechange = function(){
			if(request.readyState === 4){
				if(request.status === 200){
					var data = JSON.parse(request.responseText);
					callback(data);
				}else{
					throw new Error(request.status);
				}
			}
		}
		if(method === 'POST' || method === 'post'){
			var paramstr = paramPackage(param,'POST');
			request.open(method,url,sync);
			//不加这个参数  post请求可以发送成功  但参数接受不到
			request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			request.send(paramstr);
		}else{
			//默认get
			var paramstr = paramPackage(param,'GET');
			request.open(method,url+paramstr,sync);
			request.send();
		}
		
	}

	//获取xmlhttprequest
	function getRequest(){
		var request;
		if(window.XMLHttpRequest){
			request = new XMLHttpRequest();
		}else{
			request = new ActiveXObject('Micorosoft.XMLHTTP');
		}
		return request;
	}
    
	//拼接参数
	function paramPackage(obj,m){
		
		if(typeof(obj) != "object"){
			throw new Error("请求参数有误");
		}
		
		var paramstr = '?';
		for(var i in obj){
			paramstr += i+'='+obj[i]+'&';
		}
		
		if(paramstr.length<=1){
		 return '';
		}
		
		if(m === 'POST'){
		return paramstr.substring(1,paramstr.length-1);
		}
		return paramstr.substring(0,paramstr.length-1);
	}
	
	/*console.log("立即执行");*/
	return { ajaxRequest: ajaxRequest}
	
})();
