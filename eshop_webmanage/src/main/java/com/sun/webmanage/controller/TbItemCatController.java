package com.sun.webmanage.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.EasyUITreeData;
import com.sun.commons.utils.ActionUtils;
import com.sun.webmanage.service.TbItemCatService;

@Controller
public class TbItemCatController {

	@Resource
	private TbItemCatService tbItemCatService;
	
	@ResponseBody
	@RequestMapping("/item/cat/list")
	public List<EasyUITreeData> list(@RequestParam(defaultValue="0")long id){
		List<EasyUITreeData> listdata = tbItemCatService.listEasyUITreeData(id);
		return listdata;
	}
	
	/**
	 * 刷新分类路径
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/cat/reflashpath")
	public Map<String,Object> reflashpath(){
		tbItemCatService.setCatPath();
		return ActionUtils.ajaxSuccess("", null);
	}
}
