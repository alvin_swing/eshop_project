package com.sun.dubbo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.sun.dubbo.service.DubboTestService;
import com.sun.webmanage.mapper.TbContentCategoryMapper;
import com.sun.webmanage.model.TbContentCategory;
import com.sun.webmanage.model.TbContentCategoryExample;

public class DubboTestServiceImpl implements DubboTestService{
	
	@Resource
	private TbContentCategoryMapper tbContentCategoryMapper;

	@Override
	public List<TbContentCategory> getInfo() {
		TbContentCategoryExample example = new TbContentCategoryExample();
		List<TbContentCategory> listdata = tbContentCategoryMapper.selectByExample(example);
		return listdata;
	}

}
