package com.sun.webmanage.mapper;

import java.util.List;

import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemExample;

public interface CustomTbItemMapper {

	/**查找商品及分类信息
	 * @param example
	 * @return
	 */
	List<TbItem> selectItemAndCatByExample(TbItemExample example);
}
