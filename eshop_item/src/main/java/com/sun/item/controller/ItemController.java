package com.sun.item.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.TbItemCatCustom;
import com.sun.item.service.ItemService;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemCat;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemParamItem;

@Controller
public class ItemController {

	@Resource
	private ItemService itemService;
	
	@RequestMapping("/item/{id}.html")
	public String item(@PathVariable Long id,Model retmodel){
		TbItem item = itemService.loadTbItemById(id);
		if(item.getImage()!=null && !item.getImage().equals("")){
		item.setImages(item.getImage().split(","));
		}else{
		item.setImages(new String[0]);	
		}
		retmodel.addAttribute("item", item);
		
		//TbItemCatCustom linkCat = itemService.linkCat(item.getCid());
		//TODO 表中添加分类路径，用于显示商品分类路径信息
		
		List<TbItemCat> itemCatList = itemService.itemCatList(item.getCid());
		retmodel.addAttribute("itemCatList", itemCatList);
		return "item";
	}
	
	@ResponseBody
	@RequestMapping("/item/desc/{itemId}.html")
	public String itemDesc(@PathVariable Long itemId){
		TbItemDesc itemDesc = itemService.loadTbItemDescByItemId(itemId);
		return itemDesc.getItemDesc();
	}
	
	@ResponseBody
	@RequestMapping("/item/param/{itemId}.html")
	public String itemParam(@PathVariable Long itemId){
		TbItemParamItem itemParam = itemService.loadTbItemParamItemByItemId(itemId);
		if(itemParam==null){
			return "";
		}
		return itemService.TbItemParamHtml(itemParam.getParamData());
	}
	
	
	
}
