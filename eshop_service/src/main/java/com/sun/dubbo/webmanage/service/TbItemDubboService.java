package com.sun.dubbo.webmanage.service;

import java.util.Date;
import java.util.List;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemParamItem;

/**
 * @author sunhongmin
 * 商品信息
 */
public interface TbItemDubboService { 
	/**
	 * 商品记录
	 * @param page 请求第几页
	 * @param rows 每页多少条
	 * @return
	 */
	EasyUIDataGrid listTbItem(int page,int rows,String sort,String order);
	/**
	 * 商品上下架及逻辑删除
	 * @param ids id值，逗号分隔
	 * @param status 商品状态，1-正常，2-下架，3-删除
	 * @return 成功true或失败false
	 */
	boolean updateStatusTbItem(String ids,Byte status) throws DaoException;
	
	/**
	 * 商品信息及商品描述信息添加
	 * @param tbItem 全部字段（除id 创建时间  默认状态三个字段外所有字段）
	 * @param tbItemDesc 全部字段（除商品id 创建时间  两个字段外所有字段）
	 * @return
	 */
	boolean insertTbItemAndDesc(TbItem tbItem,TbItemDesc tbItemDesc,TbItemParamItem tbItemParamItem) throws DaoException;
	
	/**
	 * 检索指定时间范围有效数据
	 * 每次调用同步solr方法，首先调用同步日志，无记录则从最早到当前，有记录则从记录时间到当前，检索出需要添加到solr中的数据进行同步
	 * 每次更新则更新的同时将信息同步上去，每次删除则将删除的数据再solr中同步删除
	 * dubbo 提供根据时间检索的方法   提供同步solr的各种方法
	 * 考虑到dubbo的压力，于是将同步solr方法添加进后台（只有后台才对数据进行增删改，其他项目需要查只需要自行写实现就好了）
	 * @param status
	 * @param startDateTime--endDateTime
	 * @return
	 */
	List<TbItem> selectAllByStatusCreateTime(byte status,Date startDateTime,Date endDateTime);
	
	
	/**
	 * 更新商品信息
	 * @param tbItem
	 * @param tbItemDesc
	 * @param tbItemParamItem
	 * @return
	 */
	boolean updateItemRelationInfo(TbItem tbItem,TbItemDesc tbItemDesc,TbItemParamItem tbItemParamItem);
	
	
	TbItem selectTbItemByPK(Long id);
	
	boolean updateItem(TbItem tbItem);
}
